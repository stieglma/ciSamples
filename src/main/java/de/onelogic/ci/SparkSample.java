package de.onelogic.ci;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.IntStream;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.ml.Pipeline;
import org.apache.spark.ml.PipelineModel;
import org.apache.spark.ml.PipelineStage;
import org.apache.spark.ml.feature.VectorAssembler;
import org.apache.spark.ml.regression.LinearRegression;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import com.google.common.base.Charsets;

public class SparkSample implements AutoCloseable {

    public static void main(String[] args) throws Exception {
        try (SparkSample predictor = new SparkSample(); Scanner sc = new Scanner(System.in, Charsets.UTF_8.toString())) {
            sc.forEachRemaining(predictor::predict);
        }
    }

    //@formatter:off
    private static final SparkConf SPARK_CONFIG = new SparkConf()
            .setMaster("local[*]")
            .setAppName("ciSamples")
            .set("spark.sql.shuffle.partitions", "1")
            .set("spark.executor.memory", "1g")
            .set("spark.sql.autoBroadcastJoinThreshold", "1")
            .set("spark.sql.crossJoin.enabled", "true")
            .set("spark.sql.shuffle.partitions", "1");
    // @formatter:on

    private final JavaSparkContext context;
    private final SQLContext sqlContext;
    private final PipelineModel model;

    public SparkSample() {
        context = new JavaSparkContext(SPARK_CONFIG);
        sqlContext = SQLContext.getOrCreate(context.sc());

        VectorAssembler vecAss = new VectorAssembler().setInputCols(new String[] { "x" }).setOutputCol("features");
        LinearRegression linReg = new LinearRegression().setLabelCol("y").setFeaturesCol("features").setPredictionCol("predictedY");
        Pipeline pipeline = new Pipeline().setStages(new PipelineStage[] { vecAss, linReg });
        model = pipeline.fit(createTrainingData(sqlContext));
    }

    public void predict(String str) {
        tryParse(str).map(this::predictDouble).ifPresent(System.out::println);
    }

    public double predictDouble(double x) {
        return model.transform(createForecastRow(sqlContext, x)).select("predictedY").first().<Double> getAs(0);
    }

    private static Dataset<Row> createTrainingData(SQLContext sqlContext) {
        StructType schema = new StructType(new StructField[] { new StructField("x", DataTypes.DoubleType, false, Metadata.empty()),
                new StructField("y", DataTypes.DoubleType, false, Metadata.empty()) });
        List<Row> rows = new ArrayList<>();
        IntStream.range(0, 10).mapToDouble(i -> i).mapToObj(i -> RowFactory.create(i, i)).forEach(rows::add);
        return sqlContext.createDataFrame(rows, schema);
    }

    private static Dataset<Row> createForecastRow(SQLContext sqlContext, double value) {
        StructType schema = new StructType(new StructField[] { new StructField("x", DataTypes.DoubleType, false, Metadata.empty()) });
        List<Row> rows = new ArrayList<>();
        rows.add(RowFactory.create(value));
        return sqlContext.createDataFrame(rows, schema);
    }

    private static Optional<Double> tryParse(String str) {
        try {
            return Optional.of(Double.parseDouble(str));
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    @Override
    public void close() throws Exception {
        context.close();
    }

}
