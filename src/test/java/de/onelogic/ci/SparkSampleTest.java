package de.onelogic.ci;

import static com.google.common.truth.Truth.assertThat;

import org.junit.Test;


@SuppressWarnings("resource")
public class SparkSampleTest {


    @Test
    public void testMain_doubleInputs_validOutputs() {
        // Arrange
        SparkSample subject = new SparkSample();
        int x = 1;

        // Act
        double result = subject.predictDouble(x);

        // Assert
        assertThat(result).isWithin(0.0001).of(x);
    }
}
